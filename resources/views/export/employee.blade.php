<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <title>Document</title>
</head>
<body>
    <table class="table table-bordered">
        <thead>
        <tr>
            <th>ID</th>
            <th>Nama</th>
            <th>Posisi</th>
            <th>Perusahaan</th>
        </tr>
        </thead>
        <tbody>
        @foreach($employee as $num => $item)
            <tr>
                <td>{{ $num + 1 }}</td>
                <td>{{ $item->nama }}</td>
                <td>
                    @if (is_null($item->atasan_id))
                        CEO
                    @elseif ($item->atasan_id == 1)
                        Direktur
                    @elseif ($item->atasan_id == 2 || $item->atasan_id == 3)
                        Manager
                    @else
                        Staff
                    @endif
                </td>
                <td>{{ $item->company->nama }}</td>
            </tr>
        @endforeach
        </tbody>
    </table>

    <!-- Option 1: Bootstrap Bundle with Popper -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>

</body>
</html>