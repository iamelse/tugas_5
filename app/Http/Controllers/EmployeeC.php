<?php

namespace App\Http\Controllers;

use App\Exports\EmployeeExport;
use App\Models\Company;
use App\Models\Employee;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use App\Http\Controllers\Controller;

class EmployeeC extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $data['title']  = 'Pegawai';
        $employees      = Employee::all();
        return view('employees.index', ['employee' => $employees], $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    
    private function _validate(Request $request){
        $validatedData = $request->validate([
            'nama' => 'required|min:2|max:50'
        ]);
    }

    public function export(Request $request) 
    {

        //return view('export.employee', ['employee' => Employee::all()]);

        if($request->get('type') == 'excel'){
            return Excel::download(new EmployeeExport, 'employee.xlsx');
        }
        if($request->get('type') == 'pdf'){
            return Excel::download(new EmployeeExport, 'employee.pdf');
        }
        
    }

    public function create()
    {
        //
        $data['title']  = 'Pegawai - Tambah Data Pegawai';
        $company        = Company::all();
        $atasan         = Employee::all();
        return view('employees.create', ['company' => $company,'atasan' => $atasan], $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    
     public function store(Request $request)
    {
        //
        $all = $request->all();
        $this->_validate($request);
        if ($request->input('atasan_id')=='null') {
            unset($all['atasan_id']);
        }
        Employee::create($all);
        return redirect()->route('employees.index')->with('success','Data Berhasil ditambahkan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $data['title']  = 'Pegawai - Edit Data Pegawai';
        $company = Company::all();
        $atasan = Employee::all();

        $employee = Employee::find($id);
        return view('employees.edit', ['employee' => $employee, 'company' => $company, 'atasan' => $atasan], $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $all = $request->all();
        $this->_validate($request);
        $employee = Employee::find($id);
        if ($all['atasan_id']=='null') {
            unset($all['atasan_id']);
        }
        $employee->update($all);
        return redirect()->route('employees.index')->with('success','Data Berhasil diubah');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $employee = Employee::find($id);
        $employee->delete();
        return redirect()->route('employees.index')->with('success','Data Berhasil dihapus');
    }
}
